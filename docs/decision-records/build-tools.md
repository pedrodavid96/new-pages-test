# Build tools

## Decision

* Use `make` as a base tool
* Discover replicable `build environments` solutions to ensure all our
  dependencies are present on build machines or even in the cloud.

## Tools

### [`make`](./make.md)

### `fd`

`find` replacement that automagically respects `.gitignore`

### [`sass`](./sass.md)

### [Nu Html Checker](https://validator.w3.org/nu/#file)

Currently this is done manually
